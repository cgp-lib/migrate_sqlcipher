module gitee.com/cgp-lib/migrate_sqlcipher

go 1.18

require (
	gitee.com/zhujingfeng/go-sqlcipher v0.0.0-20230913041331-7e35429d1a5b
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/hashicorp/go-multierror v1.1.1
	go.uber.org/atomic v1.11.0
)

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	golang.org/x/net v0.0.0-20190225153610-fe579d43d832 // indirect
)
