package migrate_sqlcipher

import (
	"database/sql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database"
)

type SqlcipherMigrate struct {
	DriverName   string
	DSN          string
	SourceUrl    string
	DatabaseName string
	DatabasePath string
}

func NewSqlcipherMigrate(dsn, sourceUrl, databaseName, databasePath string) *SqlcipherMigrate {
	return &SqlcipherMigrate{
		DriverName:   "sqlite3",
		DSN:          dsn,
		SourceUrl:    sourceUrl,
		DatabaseName: databaseName,
		DatabasePath: databasePath,
	}
}

func (m *SqlcipherMigrate) Up() error {
	sqlcipherMigrate, err := m.NewMigrate()
	if err != nil {
		return err
	}

	err = sqlcipherMigrate.Up()
	if err != nil {
		return err
	}

	return nil
}

func (m *SqlcipherMigrate) Down() error {
	sqlcipherMigrate, err := m.NewMigrate()
	if err != nil {
		return err
	}

	err = sqlcipherMigrate.Down()
	if err != nil {
		return err
	}

	return nil
}

// Steps 调用Steps方法需要手动关闭DB,为了方便回滚
func (m *SqlcipherMigrate) Steps(n int) error {
	sqlcipherMigrate, err := m.NewMigrate()
	if err != nil {
		return err
	}

	err = sqlcipherMigrate.Steps(n)
	if err != nil {
		return err
	}

	return nil
}

func (m *SqlcipherMigrate) NewMigrate() (*migrate.Migrate, error) {
	db, err := m.OpenSqlcipher()
	if err != nil {
		return nil, err
	}

	driver, err := withInstance(db, &Config{})
	if err != nil {
		return nil, err
	}

	sqlcipherMigrate, err := newWithDatabaseInstance(m.SourceUrl, m.DatabaseName, driver)
	if err != nil {
		return nil, err
	}

	return sqlcipherMigrate, nil
}

func (m *SqlcipherMigrate) OpenSqlcipher() (*sql.DB, error) {
	db, err := sql.Open(m.DriverName, m.DSN)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func withInstance(instance *sql.DB, config *Config) (database.Driver, error) {
	if config == nil {
		return nil, ErrNilConfig
	}

	if err := instance.Ping(); err != nil {
		return nil, err
	}

	if len(config.MigrationsTable) == 0 {
		config.MigrationsTable = DefaultMigrationsTable
	}

	mx := &Sqlite{
		db:     instance,
		config: config,
	}
	if err := mx.ensureVersionTable(); err != nil {
		return nil, err
	}

	return mx, nil
}

func newWithDatabaseInstance(sourceUrl string, databaseName string, databaseInstance database.Driver) (*migrate.Migrate, error) {
	return migrate.NewWithDatabaseInstance(sourceUrl, databaseName, databaseInstance)
}
